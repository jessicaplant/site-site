jQuery( function($) {
    $( "div.module-tray" ).sortable({
        connectWith: "div"
    });
    $( "div.site-tray" ).sortable({
        connectWith: "div"
    });

    $( ".site-tray, .module-tray" ).disableSelection();
} );