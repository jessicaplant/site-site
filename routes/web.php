<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/website/{id}', 'WebsiteController@show');
//Route::get('/website/{id}/modules', 'WebsiteController@showModules');
Route::get('/website/{id}/build', 'WebsiteController@bootBuilder');
