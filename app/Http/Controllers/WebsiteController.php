<?php

namespace App\Http\Controllers;

use App\Module;
use App\Website;
use Illuminate\Http\Request;

class WebsiteController extends Controller {
	public function show( $id ) {
		$website = Website::findOrFail( $id );

		return view( 'websites/show', [ 'website' => $website ] );
	}

	public function bootBuilder( $id ) {
		$website = Website::findOrFail( $id );
		$modules = Module::all();

		return view( 'websites/build', [ 'website' => $website, 'modules' => $modules ] );
	}
}
