<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    public function user() {
	    return $this->belongsTo(User::class);
    }

    public function modules() {
	    return $this->belongsToMany(Module::class, 'websites_modules', 'website_id', 'module_id');
    }
}
