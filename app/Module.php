<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
	public function websites() {
		return $this->belongsToMany(Website::class);
	}
}
