@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    see your website



                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $website->domain }}</div>
                <div class="panel-body">
                    @foreach ($website->modules->sortBy('order_no') as $module)
                        @if ($module->filename)
                            @include('websites/modules/'.$module->filename)
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
