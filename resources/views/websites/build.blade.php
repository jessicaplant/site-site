@extends('layouts.builder')

@section('content')
<div class="container">
    <div class="row margin-20-top">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Available Modules</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="module-tray">
                        @foreach ($modules as $module)
                            @if ($module->filename)
                                @include('websites/modules/'.$module->filename)
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 fill-height">
            <div class="card fill-width fill-height white-bg box-shadow">
                <div class="site-tray">
                    @foreach ($website->modules->sortBy('order_no') as $module)
                        @if ($module->filename)
                            @include('websites/modules/'.$module->filename)
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
