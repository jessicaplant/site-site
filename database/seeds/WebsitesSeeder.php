<?php

use Illuminate\Database\Seeder;

class WebsitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    for ($i = 0; $i < 20; $i++) {
		    DB::table('websites')->insert([
			    'domain' => 'www.'.str_random(10).'co.uk',
			    'user_id' => 1
		    ]);
	    }
    }
}
