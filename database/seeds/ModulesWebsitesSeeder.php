<?php

use Illuminate\Database\Seeder;

class ModulesWebsitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    for ($i = 0; $i < 20; $i++) {
		    DB::table('websites_modules')->insert([
			    'website_id' => rand(0,2),
			    'module_id' => rand(0,19),
			    'order_no' => rand(0,4),
		    ]);
	    }
    }
}
