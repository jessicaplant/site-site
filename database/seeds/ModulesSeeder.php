<?php

use Illuminate\Database\Seeder;

class ModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    for ($i = 0; $i < 20; $i++) {
		    DB::table('modules')->insert([
			    'name' => 'Module '.$i,
			    'shortcode' => 'module_'.$i
		    ]);
	    }
    }
}
